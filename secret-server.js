const express = require('express')
const fse = require('fs-extra')
const port = 4001
const app = express()
const bodyParser = require('body-parser')

app.use(bodyParser.json())

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/views/secret-server.html')
})

app.get('/secret', (req, res) => {
    fse.readFile(__dirname + '/data/secret.txt', 'utf8', (err, data) => {
        res.writeHead(200, {'Content-Type': 'text/html'})
        res.write([...data].reverse().join(''))
        res.end()
    })
})

app.post('/', (req, res) => {
    fse.writeFile(__dirname + '/data/secret.txt', req.body.secret, (err) => {
        if (err) throw err
        console.log('New secret generated')
    })
})

app.use(
    '/client',
    express.static(__dirname + '/public')
  )

app.listen(port, () => {
    console.log(`Listening on ${port}`)
})