const express = require('express')
const port = 4002
const app = express()
const fetch = require('node-fetch')
const htmlToJson = require('html-to-json')

app.get('/:n', (req, res) => {
     const n = req.params.n; 
     if (validUrl(n)) {
        let response = []
        let interval = setInterval(function() {
           fetch('http://localhost:4000')
           .then(res => res.text())
           .then(body => {
               let promise = htmlToJson.parse(body, {
                  'text': function ($doc) {
                    const date = $doc.find('h3').text()
                    console.log('aaaaaz')
                    console.log(response)
                    response = eraseResponse(response, date)
                    //response.push(date)
                  }
                }
               )
          })
     
          fetch('http://localhost:4001/secret')
          .then(res => res.text())
          .then(body => {
              response = eraseResponse(response, body)
           })
         }, 1000)
     } else {
        res.sendStatus(404)
     }      
})

function validUrl(n) {
    if (n < 1 || n > 10 || isNaN(n)) {
        return false
    } else {
        return true  
    }
} 

function eraseResponse(response, value) {
    console.log(response)
    if (response.length == 10) {
        response.shift()
        response.push(value)
        return response
    }
    response.push(value)
    return response
}

app.listen(port, () => {
    console.log(`Listening on ${port}`)
})

