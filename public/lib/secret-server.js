document.querySelector('#ajax-form-secret-generator').addEventListener('submit', (event) => {
    event.preventDefault()
    const data = { secret: document.querySelector('#secret').value }
    fetch('/', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => response.json())
})