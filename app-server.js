const express = require('express')

const port = 5000

const app = express()

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/views/appVue.html')
})

app.use(
    '/app-server',
    express.static(__dirname + '/public')
)

app.listen(port, () => {
    console.log(`Listening on ${port}`)
})