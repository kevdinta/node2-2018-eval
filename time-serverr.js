const express = require('express')
const moment = require('moment')

const port = 4000

const app = express()

app.get('/', (req, res) => {
    if (req.accepts(['json', 'html'])) {
        const time = moment().format('hh:mm:ss')

        res
            .header({
                'Content-Type': 'text/html',
            })
            .send(`<h3 style="color: blue">${time}</h3>`)
            
    } else {
        res.sendStatus(406)
    }

})

app.listen(port, () => {
    console.log(`Listening on ${port}`)
})